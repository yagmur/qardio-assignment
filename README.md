# QARDIO - Assignment Documentation
## Build With
* [Java 11](http://www.oracle.com/technetwork/java/javase/downloads/index.html)  - Programming language
* [Maven 3.6.3](https://maven.apache.org/download.cgi) - Build tool
* [Spring boot 2.5.5](https://projects.spring.io/spring-boot/) - Spring Boot 

### Pull from git 
```
$ git clone https://gitlab.com/yagmur/qardio-assignment.git
$ cd qardio-assignment
```

### Build & run 
* Run test
```
$ mvn test
```

* Run the web server on dev mode
```
$ mvn spring-boot:run
```

### Reference Documentation
You can find sample endpoints and request/response bodies. 


The type and format of the request parameters to be sent in `POST` requests should be like this.

```   
     sensorId -> A numeric value (Long)
     temperature -> A numeric value (double)
     timestamp -> A string value, but format must be like this yyyyMMddHHmmssSSS
```

##### 1. Save Sensor Data

* Request Endpoint

    `POST http://localhost:8080/api/v1/sensor/save`
  
     `POST` data in the request need to be with single `JSON` object.
    ```
    {
      "sensorId":10000,
      "timestamp":"20210928195021000",
      "temperature":25.0
    }
    ```
* Response

    `SUCESS Response Status -> 201`

##### 2. Save Bulk Sensor Data
* Request Endpoint

    ``POST http://localhost:8080/api/v1/sensor/bulk``
    
    `POST` data in the request need to be with `JSON array`.
    ```
    [
        {
          "sensorId":15555,
          "timestamp":"20210928195021000",
          "temperature":28.0
        },
        {
          "sensorId":15555,
          "timestamp":"20210928195031000",
          "temperature":21.0
        },
        {
          "sensorId":15555,
          "timestamp":"20210928195041000",
          "temperature":29.0
        },
        {
          "sensorId":15555,
          "timestamp":"20210928195051000",
          "temperature":24.0
        }
    ]
    ```
* Response 

    ``SUCESS Response Status -> 201``


##### 3. Get Hourly Aggregation Data 
* Request Endpoint

    `id` param need to be numeric.
    
    ``GET http://localhost:8080/api/v1/sensor/hourly?id=15555``

* Response

    `Response Status` must be `200`, sample response body must be as below 
    ```
    [
        {
        "sensorId": 15555,
        "temperature": 25.0,
        "timestamp": "2021-09-28T17:50:21.000+00:00"
        }
    ]
    ```

##### 4. Get Daily Aggregation Data 
* Request Endpoint
   
    `id` param need to be numeric.
   
    ``GET http://localhost:8080/api/v1/sensor/daily?id=15555``
* Response

    `Response Status` must be `200`, sample response body must be as below 

    ```
    [
        {
        "sensorId": 15555,
        "temperature": 25.0,
        "timestamp": "2021-09-28T17:50:21.000+00:00"
        }
    ]
    ```

### Good To Know
* I created separate endpoints for saving a single data and saving bulk data scenarios. I thought that the bulk request could be sent as a `list`.

* For both scenarios, I preferred to save the data by giving it to `asynchronous` tasks in order to return to the requesting sensor as soon as possible.

* Assuming that 3th party tools will not be used in the application, I kept the data in `memory`. I did not use a `cache` library to keep it in `memory`. But as much as possible, I tried to make a design as if these 3th party tools existed.

* Code coverage is `90%` as the measure result of inteliJ Coverage. 


## Authors

* **Seyid Yagmur**
