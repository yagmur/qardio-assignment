package com.qardio.assignment.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Builder
@Getter
@Setter
public class SensorDto {
    private Long sensorId;
    private Double temperature;
    private Date timestamp;
}
