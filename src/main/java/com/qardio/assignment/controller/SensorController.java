package com.qardio.assignment.controller;


import com.qardio.assignment.dto.SensorDto;
import com.qardio.assignment.model.SensorRequest;
import com.qardio.assignment.service.SensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequestMapping("/api/v1/sensor")
public class SensorController {

    @Autowired
    private SensorService sensorService;

    @PostMapping(value = "/save")
    public ResponseEntity saveSensonData(@RequestBody @Valid SensorRequest sensorRequest){
        //I assumed that bulk request will be at the same endpoint.
        //Bulk request processing will be handled in client side
        //but I defined a new endpoint based on the client's possibility of sending a list in the bulk process.
        sensorService.saveData(sensorRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @PostMapping(value = "/bulk")
    public ResponseEntity saveSensonData(@RequestBody @NotEmpty(message = "Input list cannot be empty.") List<@Valid SensorRequest> sensorRequest){
        sensorService.saveBulkData(sensorRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @GetMapping(value = "/hourly")
    public ResponseEntity<List<SensorDto>> getHourlyStatistic(@RequestParam Long id){
        return ResponseEntity.ok(sensorService.getHourlyStatistic(id));
    }
    @GetMapping(value = "/daily")
    public ResponseEntity<List<SensorDto>> getDailyStatistic(@RequestParam Long id){
        return ResponseEntity.ok(sensorService.getDailyStatistic(id));
    }
}
