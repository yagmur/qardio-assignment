package com.qardio.assignment.dao;

import com.qardio.assignment.dto.SensorDto;

public interface TransactionDao {
    void saveTransaction(SensorDto sensorDto);
}
