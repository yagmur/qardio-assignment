package com.qardio.assignment.dao;

import com.qardio.assignment.dto.SensorDto;
import lombok.Getter;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TransactionDaoImpl implements TransactionDao{
    //I set all records to transactions object assuming to use DB.
    @Getter
    private final List<SensorDto> transactions = new ArrayList<>();
    Object lock = new Object();

    @Override
    public void saveTransaction(SensorDto sensorDto) {
        synchronized (lock) {
            transactions.add(sensorDto);
        }
    }

}
