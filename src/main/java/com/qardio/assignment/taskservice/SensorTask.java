package com.qardio.assignment.taskservice;

import com.qardio.assignment.dao.TransactionDao;
import com.qardio.assignment.dto.SensorDto;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SensorTask implements BaseTask{

    private SensorDto sensorDto;
    TransactionDao transactionDao;

    @Override
    public void run() {
        //make save transaction.
        transactionDao.saveTransaction(sensorDto);
    }
}
