package com.qardio.assignment.taskservice;

import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;

@RestControllerAdvice
public class SensorExecutorService {

    private ExecutorService executorService;

    @PostConstruct
    private void init(){
        executorService=new ThreadPoolExecutor(0,Integer.MAX_VALUE,0,
                TimeUnit.MILLISECONDS, new SynchronousQueue());
        //If I would save the data to database, I would set MAX value database's pool size.
    }
    public void executeTask(BaseTask task){
        executorService.execute(task);
    }

}
