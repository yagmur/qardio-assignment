package com.qardio.assignment.util;

import com.qardio.assignment.dto.SensorDto;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.function.Predicate;

import static com.qardio.assignment.util.Constant.COMMON_DATE_FORMAT;

public  class CommonUtil {
    public static Date tryParse(String timestamp) {
        if (timestamp.length()!=COMMON_DATE_FORMAT.length())
            return null;
        Date date;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(COMMON_DATE_FORMAT);
            date= simpleDateFormat.parse(timestamp);
        }catch (Exception e){
            return null;
        }
        return date;
    }

    public static Predicate<SensorDto> recordsIn1MinQuery(long sec)
    {
        return p ->  Duration.between(p.getTimestamp().toInstant(), Instant.now())
                .getSeconds() < sec;
    }
}
