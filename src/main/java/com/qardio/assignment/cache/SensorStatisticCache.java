package com.qardio.assignment.cache;

import com.qardio.assignment.dao.TransactionDaoImpl;
import com.qardio.assignment.dto.SensorDto;
import com.qardio.assignment.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SensorStatisticCache {
    //Assumed that I saved data into cache like redis or memcached by adding expiry date to them.
    //In this way, data will not accumulate in the cache server.
    private static final int HOUR_SEC=60*60;
    private static final int DAY_SEC=60*60*24;
    @Autowired
    private TransactionDaoImpl transactionDao;

    public List<SensorDto> getStatisticsForOneHour(Long id) {
        return transactionDao.getTransactions().stream().filter(x->x.getSensorId().equals(id)).filter(CommonUtil.recordsIn1MinQuery(HOUR_SEC)).collect(Collectors.toList());
    }
    public List<SensorDto> getStatisticsForOneDay(Long id) {
        return transactionDao.getTransactions().stream().filter(x->x.getSensorId().equals(id)).filter(CommonUtil.recordsIn1MinQuery(DAY_SEC)).collect(Collectors.toList());
    }
}
