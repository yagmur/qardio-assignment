package com.qardio.assignment.service.impl;

import com.qardio.assignment.cache.SensorStatisticCache;
import com.qardio.assignment.dao.TransactionDao;
import com.qardio.assignment.dto.SensorDto;
import com.qardio.assignment.exception.InvalidRequestException;
import com.qardio.assignment.model.SensorRequest;
import com.qardio.assignment.service.SensorService;
import com.qardio.assignment.taskservice.SensorExecutorService;
import com.qardio.assignment.taskservice.SensorTask;
import com.qardio.assignment.util.CommonUtil;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class SensorServiceImpl implements SensorService {
    @Autowired
    private SensorExecutorService executorService;
    @Autowired
    private TransactionDao transactionDao;
    @Autowired
    private SensorStatisticCache sensorStatisticCache;

    @SneakyThrows
    @Override
    public void saveData(SensorRequest request) {
        Date date=CommonUtil.tryParse(request.getTimestamp());
        if (date==null){
            throw new InvalidRequestException("Invalid date error occurred");
        }
        SensorDto sensorDto=SensorDto.builder()
                .sensorId(request.getSensorId())
                .temperature(request.getTemperature())
                .timestamp(date).build();
        executorService.executeTask(new SensorTask(sensorDto,transactionDao));//by using threadpool by implementing multi threading, we can work with higher throughput
    }

    @Override
    public void saveBulkData(List<SensorRequest> bulk) {
        CompletableFuture.runAsync(() -> {//if there are huge record, don't make wait to the client for response.
            bulk.forEach(e->{
                try {
                    saveData(e);
                }catch (Exception ex){
                    System.err.println("Exception occured during bulk request");
                }
            });
        });
    }

    @Override
    public List<SensorDto> getHourlyStatistic(Long id) {
        return sensorStatisticCache.getStatisticsForOneHour(id);
    }

    @Override
    public List<SensorDto> getDailyStatistic(Long id) {
        return sensorStatisticCache.getStatisticsForOneDay(id);
    }
}
