package com.qardio.assignment.service;

import com.qardio.assignment.dto.SensorDto;
import com.qardio.assignment.model.SensorRequest;

import java.util.List;

public interface SensorService  {
    void saveData(SensorRequest request);
    void saveBulkData(List<SensorRequest> bulk);
    List<SensorDto> getHourlyStatistic(Long id);
    List<SensorDto> getDailyStatistic(Long id);
}
