package com.qardio.assignment.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SensorRequest {
    @NonNull
    private Long sensorId;
    @NonNull
    private Double temperature;
    @NonNull
    private String timestamp;
}
