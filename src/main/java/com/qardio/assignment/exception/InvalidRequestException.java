package com.qardio.assignment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidRequestException extends Exception{
    public InvalidRequestException(String message){
        super(message);
    }
}
