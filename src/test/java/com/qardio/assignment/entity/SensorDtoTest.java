package com.qardio.assignment.entity;

import com.qardio.assignment.dto.SensorDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class SensorDtoTest {
    @Test
    public void testSensorDtoSuccessfullyCreated(){
        Date date=new Date();
        SensorDto sensorDto=SensorDto.builder()
                .sensorId(15555L).temperature(25.0).timestamp(date).build();
        assertEquals(sensorDto.getSensorId(),15555L);
        assertEquals(sensorDto.getTemperature(),25.0);
        assertEquals(sensorDto.getTimestamp(),date);
    }
    @Test
    public void testNoErrorOnMissingValue(){
        SensorDto sensorDto=SensorDto.builder()
                .sensorId(15555L).timestamp(new Date()).build();
    }
}
