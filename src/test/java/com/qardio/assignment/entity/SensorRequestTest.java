package com.qardio.assignment.entity;

import com.qardio.assignment.model.SensorRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
public class SensorRequestTest {
    @Test
    public void testSensorRequestSuccessfullyCreated(){
        String testDate="test";
        SensorRequest sensorRequest=SensorRequest.builder()
                .sensorId(15555L).temperature(25.0).timestamp(testDate).build();
        assertEquals(sensorRequest.getSensorId(),15555L);
        assertEquals(sensorRequest.getTemperature(),25.0);
        assertEquals(sensorRequest.getTimestamp(),testDate);
    }
    @Test
    public void testNoErrorOnMissingValue(){
        assertThrows( NullPointerException.class,()->SensorRequest.builder()
                .sensorId(15555L).timestamp("test").build());
    }
}
