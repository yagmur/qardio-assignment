package com.qardio.assignment.util;

import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import static com.qardio.assignment.util.Constant.COMMON_DATE_FORMAT;
import static org.junit.jupiter.api.Assertions.*;

public class CommonUtilTest {
    @Test
    public void testDateParsed(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(COMMON_DATE_FORMAT);
        Instant instant = Instant.now();
        Date date = Date.from(instant);
        String dateStr = simpleDateFormat.format(date);
        assertNotNull(CommonUtil.tryParse(dateStr));
    }
    @Test
    public void testDateNotParsed(){
        String dateStr = "test date";
        assertNull(CommonUtil.tryParse(dateStr));
    }
    @Test
    public void testDateNotParsedBecauseOfMissingDigitCount(){
        String dateStr = "0201210111521000";
        assertNull(CommonUtil.tryParse(dateStr));
    }

}
