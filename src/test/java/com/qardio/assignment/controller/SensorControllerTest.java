package com.qardio.assignment.controller;

import com.qardio.assignment.dao.TransactionDaoImpl;
import com.qardio.assignment.model.SensorRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

import static com.qardio.assignment.util.Constant.COMMON_DATE_FORMAT;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SensorControllerTest {
    @LocalServerPort
    private int port;
    private String saveURL;
    private String bulkURL;
    private String hourlyURL;
    private String dailyURL;
    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private TransactionDaoImpl transactionDao;
    @BeforeEach
    public void setUp(){
        transactionDao.getTransactions().clear();
        this.saveURL = "http://localhost:" + port + "/api/v1/sensor/save";
        this.bulkURL = "http://localhost:" + port + "/api/v1/sensor/bulk";
        this.hourlyURL = "http://localhost:" + port + "/api/v1/sensor/hourly";
        this.dailyURL = "http://localhost:" + port + "/api/v1/sensor/daily";
    }

    @Test
    public void testSaveSensorDataSuccessfully(){
        SensorRequest sensorRequest=SensorRequest.builder()
                .sensorId(15555L).temperature(25.0).timestamp("20210928011588000").build();
        ResponseEntity customResponse =
                restTemplate.postForEntity(saveURL, sensorRequest,Object.class);
        assertEquals(customResponse.getStatusCode(), HttpStatus.CREATED);

    }
    @Test
    public void testBadRequestOnSaveSensorData(){
        SensorRequest sensorRequest= new SensorRequest();
        ResponseEntity customResponse =
                restTemplate.postForEntity(saveURL, sensorRequest,Object.class);
        assertEquals(customResponse.getStatusCode(), HttpStatus.BAD_REQUEST);

    }

    @Test
    public void testInvalidRequestExceptionOnSaveSensorData(){
        SensorRequest sensorRequest=SensorRequest.builder()
                .sensorId(15555L).temperature(25.0).timestamp("0210928011588000").build();
        ResponseEntity customResponse =
                restTemplate.postForEntity(saveURL, sensorRequest,Object.class);
        assertEquals(customResponse.getStatusCode(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
    @Test
    public void testBadRequestExceptionOnBulkSensorData(){
        SensorRequest sensorRequest=new SensorRequest();
        ResponseEntity customResponse =
                restTemplate.postForEntity(bulkURL, List.of(sensorRequest),Object.class);
        assertEquals(customResponse.getStatusCode(), HttpStatus.BAD_REQUEST);
    }
    @Test
    public void testSuccessOnBulkSensorData(){
        SensorRequest sensorRequest=SensorRequest.builder()
                .sensorId(15555L).temperature(25.0).timestamp("20210928011588000").build();
        ResponseEntity customResponse =
                restTemplate.postForEntity(bulkURL, List.of(sensorRequest),Object.class);
        assertEquals(customResponse.getStatusCode(), HttpStatus.CREATED);
    }
    @Test
    public void testHourlySensorStatisticGetTrueRecords(){
        long id=16666L;
        populateSampleData(id);
        sleep(1000);
        ResponseEntity<List> customResponse =
                restTemplate.getForEntity(hourlyURL+"?id="+String.valueOf(id),List.class);
        assertEquals(customResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(customResponse.getBody().size(), 2);
    }
    @Test
    public void testDailySensorStatisticGetTrueRecords(){
        long id=16666L;
        populateSampleData(id);
        sleep(1000);
        ResponseEntity<List> customResponse =
                restTemplate.getForEntity(dailyURL+"?id="+String.valueOf(id),List.class);
        assertEquals(customResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(customResponse.getBody().size(), 2);
    }
    private void populateSampleData(long id){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(COMMON_DATE_FORMAT);
        Instant instant = Instant.now().minusSeconds(100);
        Date date = Date.from(instant);
        String dateStr = simpleDateFormat.format(date);
        SensorRequest sensorRequest1=SensorRequest.builder()
                .sensorId(id).temperature(25.0).timestamp(dateStr).build();
        SensorRequest sensorRequest2=SensorRequest.builder()
                .sensorId(id).temperature(26.0).timestamp(dateStr).build();
        SensorRequest sensorRequest3=SensorRequest.builder()
                .sensorId(id).temperature(25.0).timestamp("20200928011588000").build();
        ResponseEntity customResponse =
                restTemplate.postForEntity(bulkURL, List.of(sensorRequest1,sensorRequest2,sensorRequest3),Object.class);
    }
    protected void sleep(long duration){
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
